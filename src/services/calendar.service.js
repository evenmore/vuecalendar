
class CalendarService {
  saveEvent (event) {
    const events = this.getAllEvents()
    events.push(event)
    window.localStorage.setItem('events', JSON.stringify(events))
  }

  getEvents (startDate, endDate) {
    const allEvents = this.getAllEvents()
    return allEvents
      .filter((event) => event.date.getTime() >= startDate.getTime() && event.date.getTime() <= endDate.getTime())
  }

  getAllEvents () {
    const events = window.localStorage.getItem('events')
    return events ? JSON.parse(events).map((event) => Object.assign(event, { date: new Date(event.date) })) : []
  }
}

export default new CalendarService()
