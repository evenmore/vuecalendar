# VueCalendar

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

![](doc/screenshot-1.png?raw=true)
![](doc/screenshot-2.png?raw=true)
